import random

#find max difference between 2 elements in an array such that higher number follows the lower number
def findMaxGain(prices):
    m = 0
    diff = -1
    buy = -1
    sell = -1
    for i in range(len(prices)):
        if i == 0:
            m = prices[i]
            buy = i
            lastbuy = i
            continue
        ndiff = max(diff, prices[i]-m)
        if ndiff != diff :
            diff = ndiff
            sell = i
            buy = lastbuy
        nm = min(m, prices[i])
        if nm != m:
            m = nm
            lastbuy = i
    print(diff, buy, sell)

#shuffles a deck of card in memory in O(n)
def shuffleAndDeal():
    deck = getDeck()
    print("_____________________Before Shuffle________________________")
    for i in range(len(deck)):
        print(deck[i], end =" ")
    print("")
    m = ""
    max = 51
    iter = 0
    for i in range(len(deck)):
        iter += 1
        toReplace = random.randint(0, max);
        m = deck[toReplace]
        deck[toReplace] = deck[max]
        deck[max] = m;
        max -= 1
    
    print("_____________________After Shuffle________________________")
    for i in range(len(deck)):
        print(deck[i], end =" ")
    print("")

def getDeck():
    deck = []
    for i in range(13):
        if i == 0:
            deck.append("♠ A")
        elif i == 10:
            deck.append("♠ J")
        elif i == 11:
            deck.append("♠ Q")
        elif i == 12:
            deck.append("♠ K")
        else:
            deck.append("♠ "+str(i+1))
    for i in range(13):
        if i == 0:
            deck.append("♥ A")
        elif i == 10:
            deck.append("♥ J")
        elif i == 11:
            deck.append("♥ Q")
        elif i == 12:
            deck.append("♥ K")
        else:
            deck.append("♥ "+str(i+1))
    for i in range(13):
        if i == 0:
            deck.append("♦ A")
        elif i == 10:
            deck.append("♦ J")
        elif i == 11:
            deck.append("♦ Q")
        elif i == 12:
            deck.append("♦ K")
        else:
            deck.append("♦ "+str(i+1))
    for i in range(13):
        if i == 0:
            deck.append("♣ A")
        elif i == 10:
            deck.append("♣ J")
        elif i == 11:
            deck.append("♣ Q")
        elif i == 12:
            deck.append("♣ K")
        else:
            deck.append("♣ "+str(i+1))
    #print("♠,♥,♦,♣")
    return deck

    

if __name__ == "__main__":
    #prices = [12,21,22,7,9,32,15]
    #findMaxGain(prices)
    shuffleAndDeal();